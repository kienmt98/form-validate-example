let id = (id) => document.getElementById(id); // get id

let classes = (classes) => document.getElementsByClassName(classes);// get all class in html file


//Assign value for all id from html file
let username = id("username"),
  email = id("email"),
  password = id("password"),
  form = id("form"),
  errorMsg = classes("error"),
  successIcon = classes("success-icon"),
  failureIcon = classes("failure-icon");

  // all event listener for submit type
form.addEventListener("submit", (e) => {
  e.preventDefault(); // prevent reload website 

  // get engine from let 'engine' with three props (var, index, 'string')
  engine(username, 0, "Username cannot be blank");
  engine(email, 1, "Email cannot be blank");
  engine(password, 2, "Password cannot be blank");
});

// Assign engine with 3 props (use arrow func)
let engine = (id, serial, message) => {
  if (id.value.trim() === "") { //trim() to cut all the white space (=== '')
    errorMsg[serial].innerHTML = message; // add errorMessage in to HTML file = message
    id.style.border = "2px solid red"; // style for border when get error

    // icons
    
    failureIcon[serial].style.opacity = "1"; //Show the failure icon when get error
    successIcon[serial].style.opacity = "0";

    
  } else {
    errorMsg[serial].innerHTML = "";
    id.style.border = "2px solid green";

    // icons
    failureIcon[serial].style.opacity = "0";
    successIcon[serial].style.opacity = "1"; //Show the success Icon when success
  }
};
